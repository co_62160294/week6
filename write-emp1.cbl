       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRIRE-EMP1.
       AUTHOR. Puchong.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL.
           SELECT EMP-FILE ASSIGN TO "emp1.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
       
       DATA DIVISION. 
       FILE SECTION.
       FD  EMP-FILE.
       01  EMP-DETAILS.
           88 END-OF-EMP-FILE  VALUE HIGH-VALUE.
           05 EMP-SSN  PIC  9(9).
           05 EMP-NAME.
              10 EMP-FORENAME PIC X(10).
              10 EMP-SURNAME PIC X(15).
           05 EMP-DATA-OF-BIRTH.
              10 EMP-YOB  PIC 9(4).
              10 EMP-MOB  PIC 9(2).
              10 EMP-DOB  PIC 9(2).
           05 EMP-GENDER  PIC X.


       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT EMP-FILE 
           MOVE "123456789" TO EMP-SSN
           MOVE "PUCHONG" TO EMP-FORENAME 
           MOVE "SUMALANUKUN" TO EMP-SURNAME 
           MOVE "20001005" TO EMP-DATA-OF-BIRTH 
           MOVE "M" TO EMP-GENDER 
           WRITE EMP-DETAILS
           
           MOVE "987654321" TO EMP-SSN
           MOVE "JAKKARIN" TO EMP-FORENAME
           MOVE "SUKSAWASVHON" TO EMP-SURNAME   
           MOVE "19750413" TO EMP-DATA-OF-BIRTH 
           MOVE "M" TO EMP-GENDER 
           WRITE EMP-DETAILS

           MOVE "321654789" TO EMP-SSN
           MOVE "WORAWIT" TO EMP-FORENAME
           MOVE "WERARAN" TO EMP-SURNAME   
           MOVE "19780923" TO EMP-DATA-OF-BIRTH 
           MOVE "M" TO EMP-GENDER 
           WRITE EMP-DETAILS

           CLOSE EMP-FILE 
           GOBACK 
           .
